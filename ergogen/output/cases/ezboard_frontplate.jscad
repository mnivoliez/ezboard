function _frontplate_outline_extrude_1_6_outline_fn(){
    return new CSG.Path2D([[92.7364172,-110.1629912],[83.1287006,-55.6862492]]).appendArc([83.4721493,-55.1221097],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([122.8194659,-42.8428473]).appendArc([123.1686112,-42.3219715],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([122.832227,-38.4770827]).appendArc([123.2735648,-37.936737],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([145.1305008,-35.4394177]).appendArc([145.5737413,-34.9426498],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([145.5737413,-33.6367741]).appendArc([146.0737413,-33.1367741],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([163.0757413,-33.1367741]).appendArc([163.5757413,-33.6367741],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([163.5757413,-34.8887741]).appendArc([164.0757413,-35.3887741],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([182.0757413,-35.388774]).appendArc([182.5757413,-35.888774],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([182.5757413,-37.386774]).appendArc([183.0757413,-37.886774],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([201.0759632,-37.8867741]).appendArc([201.5759632,-38.3865521],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([201.5767413,-40.1387741]).appendPoint([201.5767413,-89.0585422]).appendArc([201.8267413,-89.4915549],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([221.9015303,-101.0817397]).appendArc([222.078122,-101.2539469],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([235.8457246,-123.7731972]).appendArc([235.7726863,-124.3875562],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([216.9238688,-143.2363737]).appendArc([216.216762,-143.2363737],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([203.8024082,-130.8220199]).appendArc([203.7146769,-130.7520894],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([172.1202974,-110.9202052]).appendArc([171.8790189,-110.8442919],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([110.4077362,-107.823198]).appendArc([110.3035865,-107.828973],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([93.3084242,-110.5697718]).appendArc([92.7364172,-110.1629912],{"radius":0.5,"clockwise":true,"large":false}).close().innerToCAG()
.subtract(
    new CSG.Path2D([[219.8979599,-137.2966767],[229.7974548,-127.3971818]]).appendPoint([219.8979599,-117.4976869]).appendPoint([209.998465,-127.3971818]).appendPoint([219.8979599,-137.2966767]).close().innerToCAG()
.union(
    new CSG.Path2D([[193.0042535,-115.0524478],[205.1286091,-122.0524478]]).appendPoint([212.1286091,-109.9280922]).appendPoint([200.0042535,-102.9280922]).appendPoint([193.0042535,-115.0524478]).close().innerToCAG()
).union(
    new CSG.Path2D([[109.7339149,-86.0985288],[123.5212234,-83.6674543]]).appendPoint([121.0901489,-69.8801458]).appendPoint([107.3028404,-72.3112203]).appendPoint([109.7339149,-86.0985288]).close().innerToCAG()
).union(
    new CSG.Path2D([[113.0332303,-104.8098761],[126.8205388,-102.3788016]]).appendPoint([124.3894643,-88.5914931]).appendPoint([110.6021558,-91.0225676]).appendPoint([113.0332303,-104.8098761]).close().innerToCAG()
).union(
    new CSG.Path2D([[91.0225676,-89.3978442],[104.8098761,-86.9667697]]).appendPoint([102.3788016,-73.1794612]).appendPoint([88.5914931,-75.6105357]).appendPoint([91.0225676,-89.3978442]).close().innerToCAG()
).union(
    new CSG.Path2D([[94.321883,-108.1091915],[108.1091915,-105.678117]]).appendPoint([105.678117,-91.8908085]).appendPoint([91.8908085,-94.321883]).appendPoint([94.321883,-108.1091915]).close().innerToCAG()
).union(
    new CSG.Path2D([[106.4345995,-67.3871815],[120.221908,-64.956107]]).appendPoint([117.7908335,-51.1687985]).appendPoint([104.003525,-53.599873]).appendPoint([106.4345995,-67.3871815]).close().innerToCAG()
).union(
    new CSG.Path2D([[87.7232522,-70.6864969],[101.5105607,-68.2554224]]).appendPoint([99.0794862,-54.4681139]).appendPoint([85.2921777,-56.8991884]).appendPoint([87.7232522,-70.6864969]).close().innerToCAG()
).union(
    new CSG.Path2D([[185.4937413,-53.7737741],[199.4937413,-53.7737741]]).appendPoint([199.4937413,-39.7737741]).appendPoint([185.4937413,-39.7737741]).appendPoint([185.4937413,-53.7737741]).close().innerToCAG()
).union(
    new CSG.Path2D([[185.4937413,-72.7737741],[199.4937413,-72.7737741]]).appendPoint([199.4937413,-58.7737741]).appendPoint([185.4937413,-58.7737741]).appendPoint([185.4937413,-72.7737741]).close().innerToCAG()
).union(
    new CSG.Path2D([[185.4937413,-91.7737741],[199.4937413,-91.7737741]]).appendPoint([199.4937413,-77.7737741]).appendPoint([185.4937413,-77.7737741]).appendPoint([185.4937413,-91.7737741]).close().innerToCAG()
).union(
    new CSG.Path2D([[166.4937413,-51.398774],[180.4937413,-51.398774]]).appendPoint([180.4937413,-37.398774]).appendPoint([166.4937413,-37.398774]).appendPoint([166.4937413,-51.398774]).close().innerToCAG()
).union(
    new CSG.Path2D([[166.4937413,-70.398774],[180.4937413,-70.398774]]).appendPoint([180.4937413,-56.398774]).appendPoint([166.4937413,-56.398774]).appendPoint([166.4937413,-70.398774]).close().innerToCAG()
).union(
    new CSG.Path2D([[166.4937413,-89.398774],[180.4937413,-89.398774]]).appendPoint([180.4937413,-75.398774]).appendPoint([166.4937413,-75.398774]).appendPoint([166.4937413,-89.398774]).close().innerToCAG()
).union(
    new CSG.Path2D([[147.4937413,-49.0237741],[161.4937413,-49.0237741]]).appendPoint([161.4937413,-35.0237741]).appendPoint([147.4937413,-35.0237741]).appendPoint([147.4937413,-49.0237741]).close().innerToCAG()
).union(
    new CSG.Path2D([[147.4937413,-68.0237741],[161.4937413,-68.0237741]]).appendPoint([161.4937413,-54.0237741]).appendPoint([147.4937413,-54.0237741]).appendPoint([147.4937413,-68.0237741]).close().innerToCAG()
).union(
    new CSG.Path2D([[147.4937413,-87.0237741],[161.4937413,-87.0237741]]).appendPoint([161.4937413,-73.0237741]).appendPoint([147.4937413,-73.0237741]).appendPoint([147.4937413,-87.0237741]).close().innerToCAG()
).union(
    new CSG.Path2D([[174.4205272,-109.3485215],[187.9434888,-112.9719881]]).appendPoint([191.5669554,-99.4490265]).appendPoint([178.0439938,-95.8255599]).appendPoint([174.4205272,-109.3485215]).close().innerToCAG()
).union(
    new CSG.Path2D([[127.7538051,-72.7014494],[141.7005309,-71.481269]]).appendPoint([140.4803505,-57.5345432]).appendPoint([126.5336247,-58.7547236]).appendPoint([127.7538051,-72.7014494]).close().innerToCAG()
).union(
    new CSG.Path2D([[126.097846,-53.7737501],[140.0445718,-52.5535697]]).appendPoint([138.8243914,-38.6068439]).appendPoint([124.8776656,-39.8270243]).appendPoint([126.097846,-53.7737501]).close().innerToCAG()
).union(
    new CSG.Path2D([[129.4097642,-91.6291487],[143.35649,-90.4089683]]).appendPoint([142.1363096,-76.4622425]).appendPoint([128.1895838,-77.6824229]).appendPoint([129.4097642,-91.6291487]).close().innerToCAG()
)).extrude({ offset: [0, 0, 1.6] });
}


function _screws_extrude_1_6_outline_fn(){
    return CAG.circle({"center":[211.3200475,-118.8192694],"radius":1.1})
.union(
    CAG.circle({"center":[166.8809563,-92.6946707],"radius":1.1})
).union(
    CAG.circle({"center":[182.9617413,-55.1347741],"radius":1.1})
).union(
    CAG.circle({"center":[107.7060159,-88.9946686],"radius":1.1})
).union(
    CAG.circle({"center":[104.4067005,-70.2833213],"radius":1.1})
).extrude({ offset: [0, 0, 1.6] });
}




                function ezboard_frontplate_case_fn() {
                    

                // creating part 0 of case ezboard_frontplate
                let ezboard_frontplate__part_0 = _frontplate_outline_extrude_1_6_outline_fn();

                // make sure that rotations are relative
                let ezboard_frontplate__part_0_bounds = ezboard_frontplate__part_0.getBounds();
                let ezboard_frontplate__part_0_x = ezboard_frontplate__part_0_bounds[0].x + (ezboard_frontplate__part_0_bounds[1].x - ezboard_frontplate__part_0_bounds[0].x) / 2
                let ezboard_frontplate__part_0_y = ezboard_frontplate__part_0_bounds[0].y + (ezboard_frontplate__part_0_bounds[1].y - ezboard_frontplate__part_0_bounds[0].y) / 2
                ezboard_frontplate__part_0 = translate([-ezboard_frontplate__part_0_x, -ezboard_frontplate__part_0_y, 0], ezboard_frontplate__part_0);
                ezboard_frontplate__part_0 = rotate([0,0,0], ezboard_frontplate__part_0);
                ezboard_frontplate__part_0 = translate([ezboard_frontplate__part_0_x, ezboard_frontplate__part_0_y, 0], ezboard_frontplate__part_0);

                ezboard_frontplate__part_0 = translate([0,0,0], ezboard_frontplate__part_0);
                let result = ezboard_frontplate__part_0;
                
            

                // creating part 1 of case ezboard_frontplate
                let ezboard_frontplate__part_1 = _screws_extrude_1_6_outline_fn();

                // make sure that rotations are relative
                let ezboard_frontplate__part_1_bounds = ezboard_frontplate__part_1.getBounds();
                let ezboard_frontplate__part_1_x = ezboard_frontplate__part_1_bounds[0].x + (ezboard_frontplate__part_1_bounds[1].x - ezboard_frontplate__part_1_bounds[0].x) / 2
                let ezboard_frontplate__part_1_y = ezboard_frontplate__part_1_bounds[0].y + (ezboard_frontplate__part_1_bounds[1].y - ezboard_frontplate__part_1_bounds[0].y) / 2
                ezboard_frontplate__part_1 = translate([-ezboard_frontplate__part_1_x, -ezboard_frontplate__part_1_y, 0], ezboard_frontplate__part_1);
                ezboard_frontplate__part_1 = rotate([0,0,0], ezboard_frontplate__part_1);
                ezboard_frontplate__part_1 = translate([ezboard_frontplate__part_1_x, ezboard_frontplate__part_1_y, 0], ezboard_frontplate__part_1);

                ezboard_frontplate__part_1 = translate([0,0,0], ezboard_frontplate__part_1);
                result = result.subtract(ezboard_frontplate__part_1);
                
            
                    return result;
                }
            
            
        
            function main() {
                return ezboard_frontplate_case_fn();
            }

        