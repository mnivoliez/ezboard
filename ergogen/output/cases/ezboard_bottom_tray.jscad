function _bottom_case_walls_extrude_6_6_outline_fn(){
    return new CSG.Path2D([[81.4792171,-54.682042],[91.5566264,-111.8219899]]).appendArc([92.1286333,-112.2287705],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([110.3843053,-109.2846901]).appendArc([110.488455,-109.2789151],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([171.4292108,-112.2739355]).appendArc([171.6704893,-112.3498488],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([202.805134,-131.8931561]).appendArc([202.8928653,-131.9630866],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([216.216762,-145.2869833]).appendArc([216.9238688,-145.2869834],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([237.6054117,-124.6054405]).appendArc([237.67845,-123.9910816],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([223.6861411,-101.104286]).appendPoint([223.7767413,-100.860165]).appendPoint([223.7767413,-39.1887741]).appendArc([223.2767413,-38.6887741],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([203.5258757,-38.6887741]).appendArc([203.0258757,-38.1889961],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([203.0257412,-37.8861302]).appendPoint([203.0253195,-36.9365521]).appendArc([202.5253195,-36.4367741],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([184.5257413,-36.436774]).appendArc([184.0257413,-35.936774],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([184.0257413,-34.438774]).appendArc([183.5257413,-33.938774],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([165.5257413,-33.9387741]).appendArc([165.0257413,-33.4387741],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([165.0257413,-32.1867741]).appendArc([164.5257413,-31.6867741],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([144.6237413,-31.6867741]).appendArc([144.1237413,-32.1867741],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([144.1237413,-33.6488891]).appendArc([143.6805008,-34.145657],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([121.7060117,-36.6564076]).appendArc([121.2646739,-37.1967534],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([121.3453065,-38.1183878]).appendPoint([121.6223961,-41.2855369]).appendArc([121.2732509,-41.8064127],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([81.8226658,-54.1179025]).appendArc([81.4792171,-54.682042],{"radius":0.5,"clockwise":false,"large":false}).close().innerToCAG()
.subtract(
    new CSG.Path2D([[82.7679203,-55.0799902],[92.6185814,-110.9342524]]).appendPoint([110.3175037,-108.0799587]).appendArc([110.4216534,-108.0741837],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([171.8014657,-111.0907822]).appendArc([172.0427442,-111.1666955],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([203.5578592,-130.9488251]).appendArc([203.6455905,-131.0187556],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([216.5703154,-143.9434805]).appendPoint([236.3670943,-124.1467016]).appendPoint([222.3025268,-101.1417149]).appendPoint([222.5767413,-100.6667614]).appendPoint([222.5767413,-39.8887741]).appendPoint([202.3264084,-39.8887741]).appendArc([201.8264084,-39.3889961],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([201.8257413,-37.8866631]).appendPoint([201.8256303,-37.6367741]).appendPoint([183.3257413,-37.636774]).appendArc([182.8257413,-37.136774],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([182.8257413,-35.138774]).appendPoint([164.3257413,-35.1387741]).appendArc([163.8257413,-34.6387741],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([163.8257413,-32.8867741]).appendPoint([145.3237413,-32.8867741]).appendPoint([145.3237413,-34.7195875]).appendArc([144.8805008,-35.2163554],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([122.519521,-37.7712655]).appendPoint([122.9020223,-42.1432759]).appendArc([122.5528771,-42.6641517],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([82.7679203,-55.0799902]).close().innerToCAG()
).extrude({ offset: [0, 0, 6.6] });
}


function _mcu_wall_cutout_extrude_5_6_outline_fn(){
    return new CSG.Path2D([[208.2727413,-68.2237741],[216.2727413,-68.2237741]]).appendPoint([216.2727413,-25.3237741]).appendPoint([208.2727413,-25.3237741]).appendPoint([208.2727413,-68.2237741]).close().innerToCAG()
.extrude({ offset: [0, 0, 5.6] });
}


function _bottom_case_outer_outline_extrude_1_outline_fn(){
    return new CSG.Path2D([[81.4792171,-54.682042],[91.5566264,-111.8219899]]).appendArc([92.1286333,-112.2287705],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([110.3843053,-109.2846901]).appendArc([110.488455,-109.2789151],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([171.4292108,-112.2739355]).appendArc([171.6704893,-112.3498488],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([202.805134,-131.8931561]).appendArc([202.8928653,-131.9630866],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([216.216762,-145.2869833]).appendArc([216.9238688,-145.2869834],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([237.6054117,-124.6054405]).appendArc([237.67845,-123.9910816],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([223.6861411,-101.104286]).appendPoint([223.7767413,-100.860165]).appendPoint([223.7767413,-39.1887741]).appendArc([223.2767413,-38.6887741],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([203.5258757,-38.6887741]).appendArc([203.0258757,-38.1889961],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([203.0257412,-37.8861302]).appendPoint([203.0253195,-36.9365521]).appendArc([202.5253195,-36.4367741],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([184.5257413,-36.436774]).appendArc([184.0257413,-35.936774],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([184.0257413,-34.438774]).appendArc([183.5257413,-33.938774],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([165.5257413,-33.9387741]).appendArc([165.0257413,-33.4387741],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([165.0257413,-32.1867741]).appendArc([164.5257413,-31.6867741],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([144.6237413,-31.6867741]).appendArc([144.1237413,-32.1867741],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([144.1237413,-33.6488891]).appendArc([143.6805008,-34.145657],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([121.7060117,-36.6564076]).appendArc([121.2646739,-37.1967534],{"radius":0.5,"clockwise":false,"large":false}).appendPoint([121.3453065,-38.1183878]).appendPoint([121.6223961,-41.2855369]).appendArc([121.2732509,-41.8064127],{"radius":0.5,"clockwise":true,"large":false}).appendPoint([81.8226658,-54.1179025]).appendArc([81.4792171,-54.682042],{"radius":0.5,"clockwise":false,"large":false}).close().innerToCAG()
.extrude({ offset: [0, 0, 1] });
}


function _screws_extrude_1_outline_fn(){
    return CAG.circle({"center":[211.3200475,-118.8192694],"radius":1.1})
.union(
    CAG.circle({"center":[166.8809563,-92.6946707],"radius":1.1})
).union(
    CAG.circle({"center":[182.9617413,-55.1347741],"radius":1.1})
).union(
    CAG.circle({"center":[107.7060159,-88.9946686],"radius":1.1})
).union(
    CAG.circle({"center":[104.4067005,-70.2833213],"radius":1.1})
).extrude({ offset: [0, 0, 1] });
}




                function ezboard_bottom_tray_case_fn() {
                    

                // creating part 0 of case ezboard_bottom_tray
                let ezboard_bottom_tray__part_0 = _bottom_case_walls_extrude_6_6_outline_fn();

                // make sure that rotations are relative
                let ezboard_bottom_tray__part_0_bounds = ezboard_bottom_tray__part_0.getBounds();
                let ezboard_bottom_tray__part_0_x = ezboard_bottom_tray__part_0_bounds[0].x + (ezboard_bottom_tray__part_0_bounds[1].x - ezboard_bottom_tray__part_0_bounds[0].x) / 2
                let ezboard_bottom_tray__part_0_y = ezboard_bottom_tray__part_0_bounds[0].y + (ezboard_bottom_tray__part_0_bounds[1].y - ezboard_bottom_tray__part_0_bounds[0].y) / 2
                ezboard_bottom_tray__part_0 = translate([-ezboard_bottom_tray__part_0_x, -ezboard_bottom_tray__part_0_y, 0], ezboard_bottom_tray__part_0);
                ezboard_bottom_tray__part_0 = rotate([0,0,0], ezboard_bottom_tray__part_0);
                ezboard_bottom_tray__part_0 = translate([ezboard_bottom_tray__part_0_x, ezboard_bottom_tray__part_0_y, 0], ezboard_bottom_tray__part_0);

                ezboard_bottom_tray__part_0 = translate([-75,0,0], ezboard_bottom_tray__part_0);
                let result = ezboard_bottom_tray__part_0;
                
            

                // creating part 1 of case ezboard_bottom_tray
                let ezboard_bottom_tray__part_1 = _mcu_wall_cutout_extrude_5_6_outline_fn();

                // make sure that rotations are relative
                let ezboard_bottom_tray__part_1_bounds = ezboard_bottom_tray__part_1.getBounds();
                let ezboard_bottom_tray__part_1_x = ezboard_bottom_tray__part_1_bounds[0].x + (ezboard_bottom_tray__part_1_bounds[1].x - ezboard_bottom_tray__part_1_bounds[0].x) / 2
                let ezboard_bottom_tray__part_1_y = ezboard_bottom_tray__part_1_bounds[0].y + (ezboard_bottom_tray__part_1_bounds[1].y - ezboard_bottom_tray__part_1_bounds[0].y) / 2
                ezboard_bottom_tray__part_1 = translate([-ezboard_bottom_tray__part_1_x, -ezboard_bottom_tray__part_1_y, 0], ezboard_bottom_tray__part_1);
                ezboard_bottom_tray__part_1 = rotate([0,0,0], ezboard_bottom_tray__part_1);
                ezboard_bottom_tray__part_1 = translate([ezboard_bottom_tray__part_1_x, ezboard_bottom_tray__part_1_y, 0], ezboard_bottom_tray__part_1);

                ezboard_bottom_tray__part_1 = translate([-75,0,1], ezboard_bottom_tray__part_1);
                result = result.subtract(ezboard_bottom_tray__part_1);
                
            

                // creating part 2 of case ezboard_bottom_tray
                let ezboard_bottom_tray__part_2 = _bottom_case_outer_outline_extrude_1_outline_fn();

                // make sure that rotations are relative
                let ezboard_bottom_tray__part_2_bounds = ezboard_bottom_tray__part_2.getBounds();
                let ezboard_bottom_tray__part_2_x = ezboard_bottom_tray__part_2_bounds[0].x + (ezboard_bottom_tray__part_2_bounds[1].x - ezboard_bottom_tray__part_2_bounds[0].x) / 2
                let ezboard_bottom_tray__part_2_y = ezboard_bottom_tray__part_2_bounds[0].y + (ezboard_bottom_tray__part_2_bounds[1].y - ezboard_bottom_tray__part_2_bounds[0].y) / 2
                ezboard_bottom_tray__part_2 = translate([-ezboard_bottom_tray__part_2_x, -ezboard_bottom_tray__part_2_y, 0], ezboard_bottom_tray__part_2);
                ezboard_bottom_tray__part_2 = rotate([0,0,0], ezboard_bottom_tray__part_2);
                ezboard_bottom_tray__part_2 = translate([ezboard_bottom_tray__part_2_x, ezboard_bottom_tray__part_2_y, 0], ezboard_bottom_tray__part_2);

                ezboard_bottom_tray__part_2 = translate([-75,0,0], ezboard_bottom_tray__part_2);
                result = result.union(ezboard_bottom_tray__part_2);
                
            

                // creating part 3 of case ezboard_bottom_tray
                let ezboard_bottom_tray__part_3 = _screws_extrude_1_outline_fn();

                // make sure that rotations are relative
                let ezboard_bottom_tray__part_3_bounds = ezboard_bottom_tray__part_3.getBounds();
                let ezboard_bottom_tray__part_3_x = ezboard_bottom_tray__part_3_bounds[0].x + (ezboard_bottom_tray__part_3_bounds[1].x - ezboard_bottom_tray__part_3_bounds[0].x) / 2
                let ezboard_bottom_tray__part_3_y = ezboard_bottom_tray__part_3_bounds[0].y + (ezboard_bottom_tray__part_3_bounds[1].y - ezboard_bottom_tray__part_3_bounds[0].y) / 2
                ezboard_bottom_tray__part_3 = translate([-ezboard_bottom_tray__part_3_x, -ezboard_bottom_tray__part_3_y, 0], ezboard_bottom_tray__part_3);
                ezboard_bottom_tray__part_3 = rotate([0,0,0], ezboard_bottom_tray__part_3);
                ezboard_bottom_tray__part_3 = translate([ezboard_bottom_tray__part_3_x, ezboard_bottom_tray__part_3_y, 0], ezboard_bottom_tray__part_3);

                ezboard_bottom_tray__part_3 = translate([-75,0,0], ezboard_bottom_tray__part_3);
                result = result.subtract(ezboard_bottom_tray__part_3);
                
            
                    return result;
                }
            
            
        
            function main() {
                return ezboard_bottom_tray_case_fn();
            }

        